import { api } from './api'


export default {
  title: 'منتورینگ',
  pageTitle: 'منتورینگ | ',
  noneToken: ['login', 'register'],
  includeKeepAliveRouters: ['login', 'register'],
  api,
}

let eni = (name, title, status) => {
  return {
    name,
    title: title || '',
    status: status || ''
  }
}

let Enum = function (items) {
  this.items = []

  items.forEach((item, index) => {
    item.value = index
    item.originalName = item.name
    this.items.push(item)
    this[item.name] = index
  })

  this.get = q =>
    (typeof q === 'number' ?
      this.items[q] :
      this.items.filter(item => item.name === q)[0]) || {}

  this.value = q => this.get(q).value
  this.name = q => this.get(q).name
  this.title = q => this.get(q).title
  this.status = q => this.get(q).status

  this.toSelectMenu = () =>
    this.items.map(item => ({
      name: item.title,
      value: item.value
    }))

  this.forEach = callback => {
    this.items.forEach(callback)
    return this
  }
  return this
}

let enums = {
  PhonetStatus: new Enum([
    eni('new', 'جدید', 'default'),
    eni('call', 'تماس', 'success'),
    eni('error', 'رد شده', 'error')
  ]),
  GenderType: new Enum([
    eni('0', 'أنثى', 'default'),
    eni('1', 'رجل', 'default'),
  ]),
  Group: new Enum([
    eni('مراجعه', 'visit', 'default'),
    eni('پایبندی', 'commitments', 'default'),
    eni('تمدید', 'renew', 'default'),
    eni('تجدید', 'restart', 'default'),
  ]),
}

export {
  enums
}

/*********************************************
 * String
 *********************************************/
if (!String.prototype.trim) {
  String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/gm, '')
  }
}
let trim = str => {
  return str.toString().replace(/^\s+|\s+$/gm, '')
}

String.prototype.toPriceFormat = function() {
  return this.replace(/(\d{3})(?=\d)/g, '$1,')
}

window.time = {
  getH: function(s) {
    if (typeof s === 'string') return parseInt(s.split(':')[0])
    else return Math.floor(s / 3600)
  },
  getM: function(s) {
    if (typeof s === 'string') {
      return parseInt(s.split(':')[1])
    } else {
      s %= 3600
      return Math.floor(s / 60)
    }
  },
  toSeconds: function(str) {
    let d = str.split(':')
    return parseInt(d[0]) * 3600 + parseInt(d[1]) * 60
  },
  toHHmm: function(seconds) {
    let h = time.getH(seconds)
    let m = time.getM(seconds)
    return (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m)
  },
  getBetween: function(a, b, stringOutput) {
    if (typeof a === 'string') a = this.toSeconds(a)
    if (typeof b === 'string') b = this.toSeconds(b)

    if (a > 86400) a /= 1000
    if (b > 86400) b /= 1000

    let total = b - a
    if (total < 0) total = 86400 - a + b
    total = Math.floor(total)
    return stringOutput ? this.toHHmm(total) : total
  }
}
window.clone = function(obj) {
  let copy

  // Handle the 3 simple types, and null or undefined
  if (null == obj || 'object' != typeof obj) return obj

  // Handle Date
  if (obj instanceof Date) {
    copy = new Date()
    copy.setTime(obj.getTime())
    return copy
  }

  // Handle Array
  if (obj instanceof Array) {
    copy = []
    for (let i = 0, len = obj.length; i < len; i++) {
      copy[i] = clone(obj[i])
    }
    return copy
  }

  // Handle Object
  if (obj instanceof Object) {
    copy = {}
    for (let attr in obj) {
      if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr])
    }
    return copy
  }

  throw new Error("Unable to copy obj! Its type isn't supported.")
}

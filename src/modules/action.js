import Vue from 'vue'
import modules from '../modules.js'
import { store } from '../store/store'

let actions = {
  loading(val) {
    if (val) {
      store.state.loadingQueue++
    } else {
      setTimeout(() => {
        store.state.loadingQueue--
        if (store.state.loadingQueue < 0) store.state.loadingQueue = 0
      }, 100)
    }
  },
  showRetry(callback, message) {
    modules.state.retry = {
      show: true,
      message,
      onRetry() {
        modules.state.retry = {}
        if (callback && typeof callback === 'function') {
          callback()
        }
      }
    }
  },

}

export default actions

import './assets/sass/_bootstrap.scss'
import './assets/sass/style.scss'
import './modules/utils'
import 'core-js'
import 'babel-polyfill'

import Vue from 'vue'
import App from './App.vue'
import { moment } from './modules/moment'
import './modules/helper'

import VuePersianDatetimePicker from 'vue-persian-datetime-picker'
import vueDirectiveTooltip from './plugins/vueDirectiveTooltip'

Vue.prototype.$moment = moment

Vue.prototype.$parents = function(parentName) {
  parentName = parentName.toLowerCase()
  let parent = this
  const pName = function(vm) {
    if (vm && vm.$options && vm.$options.name) {
      return vm.$options.name.toLowerCase()
    } else {
      return ''
    }
  }
  while (parent && pName(parent) !== parentName) {
    parent = parent.$parent
  }
  return parent
}
Vue.prototype.$baseUrl = window.location.origin

Vue.use(vueDirectiveTooltip, {
  delay: 700,
  placement: 'auto',
  class: '',
  triggers: ['hover', 'focus'],
  offset: 0
})

Vue.use(clone(VuePersianDatetimePicker), {
  name: 'date-picker',
  props: {
    format: 'YYYY-M-D',
    altFormat: 'YYYY-MM-DD',
    displayFormat: 'jYYYY/jM/jD',
    color: '#0d83dd',
    autoSubmit: true
  }
})
Vue.use(clone(VuePersianDatetimePicker), {
  name: 'time-picker',
  props: {
    type: 'time',
    format: 'HH:mm:ss',
    altFormat: 'HH:mm:ss',
    displayFormat: 'HH:mm:ss',
    color: '#0d83dd'
  }
})

import { store } from './store/store'
/**********************************************
 * helpers and tools
 *********************************************/

/*********************************************
 * plugins
 *********************************************/
import Modal from './plugins/modal'
import VueBar from 'vuebar'
import VueIziToast from 'vue-izitoast'
import VuePlyr from 'vue-plyr'
import 'izitoast/dist/css/iziToast.css'
import 'vue-plyr/dist/vue-plyr.css'
// Vue.use(VueTour)
Vue.use(VuePlyr, {
  plyr: {}
})
Vue.use(Modal)
Vue.use(VueBar)
Vue.use(VueIziToast, {
  rtl: true,
  timeout: 10000
})
/**********************************************
 * Store
 *********************************************/
import VueStore from './store/vueStore'
import modules from './modules'
Vue.use(VueStore, modules)
/**********************************************
 * cookie
 *********************************************/
import VueCookie from 'vue-cookie'

Vue.use(VueCookie)
/**********************************************
 * Vue Validate
 *********************************************/
import VeeValidate from 'vee-validate'
import { validatorOptions } from './modules/validator'

Vue.use(VeeValidate, validatorOptions)

/**********************************************
 * Vue Filters
 *********************************************/
import filters from './modules/vueFilters'

Vue.filter('date', filters.dateFormat)
Vue.filter('time', filters.timeFormat)

/**********************************************
 * Vue Directives
 *********************************************/
import sticky from './plugins/vue-sticky-directive'
import directives from './modules/vueDirectives'
Vue.directive('number', directives.number)
Vue.directive('focus', directives.focus)
Vue.directive('checkMaxLength', directives.checkMaxLength)
Vue.directive('sticky', sticky)

/**********************************************
 * router and vue router
 *********************************************/
import {router, VueRouter} from './router/router'
Vue.use(VueRouter)

/*********************************************
 * THE APP
 *********************************************/
Vue.config.productionTip = false
Vue.config.warnHandler = () => {}
let APP = new Vue({
  el: '#app',
  router,
  store,
  components: {},
  directives: {},
  render: h => h(App)
})

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    loading: false,
    loadingQueue: 0,
    loadingMessages: false,
    retry: {},
    notificationSystem: {
      options: {
        success: {
          position: 'bottomRight',
          timeout: 3000
        },
        error: {
          position: 'bottomRight',
          timeout: 3000
        }
      }
    },
    userInfo: []
  },
  getters: {},
  mutations: {},
  actions: {},
  modules: {
  }
})

import {asyncRequire} from './modules'

const Header = asyncRequire('includes/Header')
const RightSidebar = asyncRequire('includes/Sidebar/Sidebar')
const RightSidebarOverlay = asyncRequire('includes/Sidebar/SidebarOverlay')
const Footer = asyncRequire('includes/Footer')

//Main Component
const Home = asyncRequire('Home/Home')
//Master Component
//other page
const NotFound = asyncRequire('pages/NotFound')

const getComponents = (def, header, rightSidebar, footer) => {
  let obj = {default: def}
  if (header !== false) obj.header = Header
  if (rightSidebar !== false) {
    obj['right-sidebar'] = RightSidebar
    obj['right-sidebar-o'] = RightSidebarOverlay
  }
  if (footer !== false) obj['footer'] = Footer
  return obj
}
//route use header and footer
const getRoute = (base, component, name, title, header, rightSidebar, footer) => {
  return {
    path: base,
    components: getComponents(component, header, rightSidebar, footer),
    name: name,
    meta: {
      pageTitle: title,
      base: base
    }
  }
}
//route dont use header and footer
const getSingleRoute = (base, component, name, title) => {
  return {
    path: base,
    component: component,
    name: name,
    meta: {
      pageTitle: title,
      base: base
    }
  }
}

//create path route
const routePath = {
  Home: '/',
  NotFount: '/*'
}

//هinstalation route
export const routes = [
  getRoute(routePath.Home, Home, 'خانه', 'خانه'),
  getSingleRoute(routePath.NotFount, NotFound, 'NotFound', '404')
]

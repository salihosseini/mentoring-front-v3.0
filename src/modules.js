import { settings } from './modules/settings'
import apilist from './config/apiList'
import config from './config/config'
import { enums } from './modules/enum'
import actions from './modules/action'

const modules = {
  settings,
  apilist,
  config,
  enums,
  actions,
}
export default modules
